/*
Table: Logs

+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| num         | varchar |
+-------------+---------+
id is the primary key for this table.
 

Write an SQL query to find all numbers that appear at least three times consecutively.

Return the result table in any order.

The query result format is in the following example.

 

Example 1:

Input: 
Logs table:
+----+-----+
| id | num |
+----+-----+
| 1  | 1   |
| 2  | 1   |
| 3  | 1   |
| 4  | 2   |
| 5  | 1   |
| 6  | 2   |
| 7  | 2   |
+----+-----+
Output: 
+-----------------+
| ConsecutiveNums |
+-----------------+
| 1               |
+-----------------+
Explanation: 1 is the only number that appears consecutively for at least three times.
*/

SELECT DISTINCT
Num AS ConsecutiveNums
FROM(
SELECT
Num,
LEAD(Num,1) OVER (Order by Id) as lead_1,
LEAD(Num,2) OVER (Order by Id) as lead_2
FROM Logs) a
WHERE Num=lead_1 AND lead_1=lead_2
/*
{"headers": ["NUM", "LEAD_1", "LEAD_2"], "values": 
[[1, 1, 1], 
 [1, 1, 2], 
 [1, 2, 1], 
 [2, 1, 2], 
 [1, 2, 2], 
 [2, 2, null], 
 [2, null, null]]}
*/

