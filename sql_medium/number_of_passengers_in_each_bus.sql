/* 
Table: Buses

+--------------+------+
| Column Name  | Type |
+--------------+------+
| bus_id       | int  |
| arrival_time | int  |
+--------------+------+
bus_id is the primary key column for this table.
Each row of this table contains information about the arrival time of a bus at the LeetCode station.
No two buses will arrive at the same time.
 

Table: Passengers

+--------------+------+
| Column Name  | Type |
+--------------+------+
| passenger_id | int  |
| arrival_time | int  |
+--------------+------+
passenger_id is the primary key column for this table.
Each row of this table contains information about the arrival time of a passenger at the LeetCode station.
 

Buses and passengers arrive at the LeetCode station. If a bus arrives at the station at time tbus and a passenger arrived at time tpassenger where tpassenger <= tbus and the passenger did not catch any bus, the passenger will use that bus.

Write an SQL query to report the number of users that used each bus.

Return the result table ordered by bus_id in ascending order.

The query result format is in the following example.

 

Example 1:

Input: 
Buses table:
+--------+--------------+
| bus_id | arrival_time |
+--------+--------------+
| 1      | 2            |
| 2      | 4            |
| 3      | 7            |
+--------+--------------+
Passengers table:
+--------------+--------------+
| passenger_id | arrival_time |
+--------------+--------------+
| 11           | 1            |
| 12           | 5            |
| 13           | 6            |
| 14           | 7            |
+--------------+--------------+
Output: 
+--------+----------------+
| bus_id | passengers_cnt |
+--------+----------------+
| 1      | 1              |
| 2      | 0              |
| 3      | 3              |
+--------+----------------+
Explanation: 
- Passenger 11 arrives at time 1.
- Bus 1 arrives at time 2 and collects passenger 11.

- Bus 2 arrives at time 4 and does not collect any passengers.

- Passenger 12 arrives at time 5.
- Passenger 13 arrives at time 6.
- Passenger 14 arrives at time 7.
- Bus 3 arrives at time 7 and collects passengers 12, 13, and 14.
*/


/* Write your PL/SQL query statement below */

with cte as 
(
    select
        b.bus_id,
        b.arrival_time,
        p.passenger_id,
        p.arrival_time passenger_at,
        rank() over(partition by passenger_id order by  b.arrival_time) rnk
        from buses b cross join passengers p
    where b.arrival_time>=p.arrival_time
)
select 
    b.bus_id,
    sum (case when rnk =1 then 1 else 0 end) passengers_cnt 
from 
    cte c right join buses b on b.bus_id = c.bus_id
group by b.bus_id
order by 1


full join result
{"headers": ["BUS_ID", "ARRIVAL_TIME", "PASSENGER_ID", "PASSENGER_AT", "RNK"], "values": 
[[1, 2, 11, 1, 1], 
 [2, 4, 11, 1, 2], 
 [3, 7, 11, 1, 3], 
 [3, 7, 12, 5, 1], 
 [3, 7, 13, 6, 1], 
 [3, 7, 14, 7, 1]]}

 