/*
Table: Activity

+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| player_id    | int     |
| device_id    | int     |
| event_date   | date    |
| games_played | int     |
+--------------+---------+
(player_id, event_date) is the primary key of this table.
This table shows the activity of players of some games.
Each row is a record of a player who logged in and played a number of games (possibly 0) before logging out on someday using some device.
 

Write an SQL query to report the fraction of players that logged in again on the day after the day they first logged in, rounded to 2 decimal places. In other words, you need to count the number of players that logged in for at least two consecutive days starting from their first login date, then divide that number by the total number of players.

The query result format is in the following example.

 

Example 1:

Input: 
Activity table:
+-----------+-----------+------------+--------------+
| player_id | device_id | event_date | games_played |
+-----------+-----------+------------+--------------+
| 1         | 2         | 2016-03-01 | 5            |
| 1         | 2         | 2016-03-02 | 6            |
| 2         | 3         | 2017-06-25 | 1            |
| 3         | 1         | 2016-03-02 | 0            |
| 3         | 4         | 2018-07-03 | 5            |
+-----------+-----------+------------+--------------+
Output: 
+-----------+
| fraction  |
+-----------+
| 0.33      |
+-----------+
Explanation: 
Only the player with id 1 logged back in after the first day he had logged in so the answer is 1/3 = 0.33
*/

/* Write your PL/SQL query statement below */

/* this doesnt work as it says starting from first login date - so use min 
with cte as (
    select count(distinct player_id) as i
    from 
        (select player_id, event_date, 
        lead(Event_date) over (partition by player_id order by event_date) as l ,
         count(distinct player_id)
        from activity  group by player_id, event_date) a
        where event_date + 1 = l )
        
select round(i/(select count(distinct player_id) from activity), 2) as fraction from cte;
*/

WITH tb1 AS
(SELECT player_id, min(event_date) event_date FROM activity GROUP BY player_id),

nextday AS
(SELECT count(distinct tb1.player_id) no_next_day
FROM activity act, tb1 WHERE act.player_id = tb1.player_id AND act.event_date = tb1.event_date+1),

total AS
(SELECT count(DISTINCT act.player_id) tot_no FROM activity act)

select
ROUND((no_next_day/tot_no), 2) as fraction
FROM
nextday,
total;