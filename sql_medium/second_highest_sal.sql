/* Table: Employee

+-------------+------+
| Column Name | Type |
+-------------+------+
| id          | int  |
| salary      | int  |
+-------------+------+
id is the primary key column for this table.
Each row of this table contains information about the salary of an employee.
 

Write an SQL query to report the second highest salary from the Employee table. If there is no second highest salary, the query should report null.*/

select max(tb1.Salary) as SecondHighestSalary
from 
    (select Salary, dense_rank() over(order by Salary desc) as rank
    from Employee) tb1
where tb1.rank = 2

select (
  select distinct Salary from Employee order by Salary Desc limit 1 offset 1
)as second
Change the number after 'offset' gives u n-th highest salary    