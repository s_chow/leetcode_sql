/*
A table contains transaction information about various products where the information such as transaction date, product sold and transaction amount 
is stored.

Produce an output to display the consolidated transaction details of each product by date along with month to date and year to date 
consolidated transaction amounts.

Transactions:
+------------+-------- ---+-----------+
| TxnDate    | Product    | TxnAmount |
+------------+------- ----+-----------+
| 2020-01-01 | Chair      | 23.01     |
| 2020-01-01 | Chair      | 32.02     |
| 2020-01-02 | Chair      | 56.04     |
| 2020-01-03 | Chair      | 34.37     |
| 2020-01-03 | Chair      | 65.82     |
| 2020-01-04 | Chair      | 90.26     |
| 2020-01-05 | Chair      | 32.14     |
| 2020-01-06 | Chair      | 12.72     |
| 2020-01-06 | Chair      | 65.92     |
| 2020-02-01 | Chair      | 73.14     |
| 2020-02-01 | Chair      | 27.28     |
| 2020-02-04 | Chair      | 92.94     |
| 2020-02-04 | Chair      | 63.15     |
| 2020-02-05 | Chair      | 94.42     |
| 2021-01-01 | Chair      | 27.64     |
| 2021-01-02 | Chair      | 61.18     |
| 2021-01-03 | Chair      | 91.43     |
| 2021-02-01 | Chair      | 82.66     |
| 2021-02-01 | Chair      | 95.23     |
| 2021-02-01 | Chair      | 9.00      |
| 2020-01-01 | Table Set  | 27.54     |
| 2020-01-02 | Table Set  | 34.26     |
| 2020-01-03 | Table Set  | 27.73     |
| 2020-01-04 | Table Set  | 90.52     |
| 2020-01-05 | Table Set  | 28.81     |
| 2020-01-05 | Table Set  | 82.11     |
| 2020-01-05 | Table Set  | 23.15     |
| 2020-01-05 | Table Set  | 22.36     |
| 2020-01-05 | Table Set  | 64.72     |
| 2020-02-01 | Table Set  | 47.36     |
| 2020-02-02 | Table Set  | 93.33     |
| 2020-02-03 | Table Set  | 89.66     |
| 2020-02-05 | Table Set  | 46.47     |
| 2020-02-07 | Table Set  | 29.82     |
| 2021-01-03 | Table Set  | 93.90     |
| 2021-01-04 | Table Set  | 1.01      |
| 2021-01-05 | Table Set  | 23.22     |
+------------+------------+-----------+

Expected output:
+------------+------------+-----------+-------------+------------+
| TxnDate    | Product    | TxnAmount | MonthToDate | YearToDate |
+------------+------------+-----------+-------------+------------+
| 2020-01-01 | Chair      | 55.03     | 55.03       | 55.03      |
| 2020-01-02 | Chair      | 56.04     | 111.07      | 111.07     |
| 2020-01-03 | Chair      | 100.19    | 211.26      | 211.26     |
| 2020-01-04 | Chair      | 90.26     | 301.52      | 301.52     |
| 2020-01-05 | Chair      | 32.14     | 333.66      | 333.66     |
| 2020-01-06 | Chair      | 78.64     | 412.3       | 412.3      |
| 2020-02-01 | Chair      | 100.42    | 100.42      | 512.72     |
| 2020-02-04 | Chair      | 156.09    | 256.51      | 668.81     |
| 2020-02-05 | Chair      | 94.42     | 350.93      | 763.23     |
| 2021-01-01 | Chair      | 27.64     | 27.64       | 27.64      |
| 2021-01-02 | Chair      | 61.18     | 88.82       | 88.82      |
| 2021-01-03 | Chair      | 91.43     | 180.25      | 180.25     |
| 2021-02-01 | Chair      | 186.89    | 186.89      | 367.14     |
| 2020-01-01 | Table Set  | 27.54     | 27.54       | 27.54      |
| 2020-01-02 | Table Set  | 34.26     | 61.8        | 61.8       |
| 2020-01-03 | Table Set  | 27.73     | 89.53       | 89.53      |
| 2020-01-04 | Table Set  | 90.52     | 180.05      | 180.05     |
| 2020-01-05 | Table Set  | 221.15    | 401.2       | 401.2      |
| 2020-02-01 | Table Set  | 47.36     | 47.36       | 448.56     |
| 2020-02-02 | Table Set  | 93.33     | 140.69      | 541.89     |
| 2020-02-03 | Table Set  | 89.66     | 230.35      | 631.55     |
| 2020-02-05 | Table Set  | 46.47     | 276.82      | 678.02     |
| 2020-02-07 | Table Set  | 29.82     | 306.64      | 707.84     |
| 2021-01-03 | Table Set  | 93.9      | 93.9        | 93.9       |
| 2021-01-04 | Table Set  | 1.01      | 94.91       | 94.91      |
| 2021-01-05 | Table Set  | 23.22     | 118.13      | 118.13     |
+------------+------------+-----------+-------------+------------+
 */


 WITH LOG_TABLE (TXNDATE,PRODUCT,TXNAMOUNT)
AS (VALUES
('2020-01-01','CHAIR', 23.01),
('2020-01-01','CHAIR', 32.02),
('2020-01-02','CHAIR', 56.04),
('2020-01-03','CHAIR', 34.37),
('2020-01-03','CHAIR', 65.82),
('2020-01-04','CHAIR', 90.26),
('2020-01-05','CHAIR', 32.14),
('2020-01-06','CHAIR', 12.72),
('2020-01-06','CHAIR', 65.92),
('2020-02-01','CHAIR', 73.14),
('2020-02-01','CHAIR', 27.28),
('2020-02-04','CHAIR', 92.94),
('2020-02-04','CHAIR', 63.15),
('2020-02-05','CHAIR', 94.42),
('2021-01-01','CHAIR', 27.64),
('2021-01-02','CHAIR', 61.18),
('2021-01-03','CHAIR', 91.43),
('2021-02-01','CHAIR', 82.66),
('2021-02-01','CHAIR', 95.23),
('2021-02-01','CHAIR', 9.00 ),
('2020-01-01','TABLE SET', 27.54),
('2020-01-02','TABLE SET', 34.26),
('2020-01-03','TABLE SET', 27.73),
('2020-01-04','TABLE SET', 90.52),
('2020-01-05','TABLE SET', 28.81),
('2020-01-05','TABLE SET', 82.11),
('2020-01-05','TABLE SET', 23.15),
('2020-01-05','TABLE SET', 22.36),
('2020-01-05','TABLE SET', 64.72),
('2020-02-01','TABLE SET', 47.36),
('2020-02-02','TABLE SET', 93.33),
('2020-02-03','TABLE SET', 89.66),
('2020-02-05','TABLE SET', 46.47),
('2020-02-07','TABLE SET', 29.82),
('2021-01-03','TABLE SET', 93.90),
('2021-01-04','TABLE SET', 1.01 ),
('2021-01-05','TABLE SET', 23.22)),
REC AS(
SELECT
TXNDATE,
PRODUCT,
SUM(TXNAMOUNT) TXNAMOUNT
FROM LOG_TABLE
GROUP BY TXNDATE,PRODUCT),
RESULT AS(
SELECT
TXNDATE,
PRODUCT,
TXNAMOUNT,
SUM(TXNAMOUNT) OVER(PARTITION BY SUBSTR(TXNDATE,1,4),SUBSTR(TXNDATE,6,2),PRODUCT ORDER BY PRODUCT,TXNDATE) MONTHTODATE,
SUM(TXNAMOUNT) OVER(PARTITION BY SUBSTR(TXNDATE,1,4),PRODUCT ORDER BY PRODUCT,TXNDATE) YEARTODATE
FROM REC
)
SELECT
TXNDATE,
PRODUCT,
TXNAMOUNT,
MONTHTODATE,
YEARTODATE
FROM RESULT
ORDER BY PRODUCT,TXNDATE;