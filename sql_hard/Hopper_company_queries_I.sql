/*
Table: Drivers

+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| driver_id   | int     |
| join_date   | date    |
+-------------+---------+
driver_id is the primary key for this table.
Each row of this table contains the driver's ID and the date they joined the Hopper company.
 

Table: Rides

+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| ride_id      | int     |
| user_id      | int     |
| requested_at | date    |
+--------------+---------+
ride_id is the primary key for this table.
Each row of this table contains the ID of a ride, the user's ID that requested it, and the day they requested it.
There may be some ride requests in this table that were not accepted.
 

Table: AcceptedRides

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| ride_id       | int     |
| driver_id     | int     |
| ride_distance | int     |
| ride_duration | int     |
+---------------+---------+
ride_id is the primary key for this table.
Each row of this table contains some information about an accepted ride.
It is guaranteed that each accepted ride exists in the Rides table.
 

Write an SQL query to report the following statistics for each month of 2020:

The number of drivers currently with the Hopper company by the end of the month (active_drivers).
The number of accepted rides in that month (accepted_rides).
Return the result table ordered by month in ascending order, where month is the month's number (January is 1, February is 2, etc.).

The query result format is in the following example.

 

Example 1:

Input: 
Drivers table:
+-----------+------------+
| driver_id | join_date  |
+-----------+------------+
| 10        | 2019-12-10 |
| 8         | 2020-1-13  |
| 5         | 2020-2-16  |
| 7         | 2020-3-8   |
| 4         | 2020-5-17  |
| 1         | 2020-10-24 |
| 6         | 2021-1-5   |
+-----------+------------+
Rides table:
+---------+---------+--------------+
| ride_id | user_id | requested_at |
+---------+---------+--------------+
| 6       | 75      | 2019-12-9    |
| 1       | 54      | 2020-2-9     |
| 10      | 63      | 2020-3-4     |
| 19      | 39      | 2020-4-6     |
| 3       | 41      | 2020-6-3     |
| 13      | 52      | 2020-6-22    |
| 7       | 69      | 2020-7-16    |
| 17      | 70      | 2020-8-25    |
| 20      | 81      | 2020-11-2    |
| 5       | 57      | 2020-11-9    |
| 2       | 42      | 2020-12-9    |
| 11      | 68      | 2021-1-11    |
| 15      | 32      | 2021-1-17    |
| 12      | 11      | 2021-1-19    |
| 14      | 18      | 2021-1-27    |
+---------+---------+--------------+
AcceptedRides table:
+---------+-----------+---------------+---------------+
| ride_id | driver_id | ride_distance | ride_duration |
+---------+-----------+---------------+---------------+
| 10      | 10        | 63            | 38            |
| 13      | 10        | 73            | 96            |
| 7       | 8         | 100           | 28            |
| 17      | 7         | 119           | 68            |
| 20      | 1         | 121           | 92            |
| 5       | 7         | 42            | 101           |
| 2       | 4         | 6             | 38            |
| 11      | 8         | 37            | 43            |
| 15      | 8         | 108           | 82            |
| 12      | 8         | 38            | 34            |
| 14      | 1         | 90            | 74            |
+---------+-----------+---------------+---------------+
Output: 
+-------+----------------+----------------+
| month | active_drivers | accepted_rides |
+-------+----------------+----------------+
| 1     | 2              | 0              |
| 2     | 3              | 0              |
| 3     | 4              | 1              |
| 4     | 4              | 0              |
| 5     | 5              | 0              |
| 6     | 5              | 1              |
| 7     | 5              | 1              |
| 8     | 5              | 1              |
| 9     | 5              | 0              |
| 10    | 6              | 0              |
| 11    | 6              | 2              |
| 12    | 6              | 1              |
+-------+----------------+----------------+
Explanation: 
By the end of January --> two active drivers (10, 8) and no accepted rides.
By the end of February --> three active drivers (10, 8, 5) and no accepted rides.
By the end of March --> four active drivers (10, 8, 5, 7) and one accepted ride (10).
By the end of April --> four active drivers (10, 8, 5, 7) and no accepted rides.
By the end of May --> five active drivers (10, 8, 5, 7, 4) and no accepted rides.
By the end of June --> five active drivers (10, 8, 5, 7, 4) and one accepted ride (13).
By the end of July --> five active drivers (10, 8, 5, 7, 4) and one accepted ride (7).
By the end of August --> five active drivers (10, 8, 5, 7, 4) and one accepted ride (17).
By the end of September --> five active drivers (10, 8, 5, 7, 4) and no accepted rides.
By the end of October --> six active drivers (10, 8, 5, 7, 4, 1) and no accepted rides.
By the end of November --> six active drivers (10, 8, 5, 7, 4, 1) and two accepted rides (20, 5).
By the end of December --> six active drivers (10, 8, 5, 7, 4, 1) and one accepted ride (2).
*/

/* Write your PL/SQL query statement below */

with activeusers as (
select 1 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-01-31','yyyy-mm-dd')
union
select 2 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-02-29','yyyy-mm-dd')
union
select 3 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-03-31','yyyy-mm-dd')
union
select 4 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-04-30','yyyy-mm-dd')
union
select 5 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-05-31','yyyy-mm-dd')
union
select 6 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-06-30','yyyy-mm-dd')
union
select 7 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-07-31','yyyy-mm-dd')
union
select 8 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-08-31','yyyy-mm-dd')
union
select 9 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-09-30','yyyy-mm-dd')
union
select 10 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-10-31','yyyy-mm-dd')
union
select 11 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-11-30','yyyy-mm-dd')
union
select 12 as month,count(distinct driver_id) as active_drivers from Drivers where join_date <= to_date('2020-12-31','yyyy-mm-dd')
)
, tbl as(
select to_number ((to_char(rd.requested_at,'mm')),'99') as month ,count(ar.ride_id) as accepted_rides
from AcceptedRides ar join Rides rd
on ar.ride_id=rd.ride_id
where to_char(rd.requested_at,'yyyy')='2020'
group by to_number ((to_char(rd.requested_at,'mm')),'99')
)

select au.month as month,active_drivers,nvl(accepted_rides,0) as accepted_rides
from activeusers au left join tbl ar
on au.month=ar.month
order by au.month asc

/*below 2020 dataset

select rownum as "month", 
to_date(rownum, 'MM'), 
last_day(to_date(rownum, 'MM') - 365) as last_day_month
    from dual
    connect by rownum <= 12
    
    
    {"headers": ["month", "TO_DATE(ROWNUM,'MM')", "LAST_DAY_MONTH"], "values":
    [[1, "2022-01-01 00:00:00", "2021-01-31 00:00:00"], 
     [2, "2022-02-01 00:00:00", "2021-02-28 00:00:00"], [
     3, "2022-03-01 00:00:00", "2021-03-31 00:00:00"], [4, "2022-04-01 00:00:00", "2021-04-30 00:00:00"], [5, "2022-05-01 00:00:00", "2021-05-31 00:00:00"], [6, "2022-06-01 00:00:00", "2021-06-30 00:00:00"], [7, "2022-07-01 00:00:00", "2021-07-31 00:00:00"], [8, "2022-08-01 00:00:00", "2021-08-31 00:00:00"], [9, "2022-09-01 00:00:00", "2021-09-30 00:00:00"], [10, "2022-10-01 00:00:00", "2021-10-31 00:00:00"], [11, "2022-11-01 00:00:00", "2021-11-30 00:00:00"], [12, "2022-12-01 00:00:00", "2021-12-31 00:00:00"]]}

     */
     
/*
with accepted_ride_2020 as (
    select a.ride_id, to_char(a.requested_at, 'MM') as month, b.driver_id
    from Rides a join AcceptedRides b
    on a.ride_id = b.ride_id
    where to_char(a.requested_at, 'YYYY') = '2020'
),

months_2020 as (
    select rownum as "month", last_day(to_date(rownum, 'MM') - 365) as last_day_month
    from dual
    connect by rownum <= 12
),

ride_by_month as(
    select "month",  b.driver_id, c.ride_id
    from months_2020 a left join drivers b
    on b.join_date <= a.last_day_month
    left join accepted_ride_2020 c
    on to_char(a.last_day_month, 'MM') = c.month
)

select "month", 
       count(distinct driver_id) as "active_drivers",
       count(distinct ride_id) as "accepted_rides"
from ride_by_month
group by "month"*/

/*
WITH months as (
    SELECT ROWNUM r FROM DUAL CONNECT BY ROWNUM <= 12 
),
cte as (
    SELECT      driver_id
                ,CASE 
                    WHEN extract(YEAR FROM join_date) = 2019 THEN 1 
                    ELSE extract(month FROM join_date) END as month
        FROM drivers
        WHERE extract(YEAR FROM join_date) <= 2020
),
cte1 as (

    SELECT t1.r as month
        ,t2.driver_id
    FROM months t1
    LEFT JOIN cte t2
    ON t1.r = t2.month
)
,cte2 as (
SELECT DISTINCT month
     ,count(driver_id) OVER(ORDER BY month) as active_drivers
    FROM cte1

)
, cte3 as(
SELECT EXTRACT(MONTH FROM t1.requested_at) as month
     ,count(*) as accepted_rides
FROM Rides t1
INNER JOIN AcceptedRides t2
ON t1.ride_id = t2.ride_id
WHERE EXTRACT(YEAR FROM t1.requested_at) = 2020
GROUP BY EXTRACT(MONTH FROM t1.requested_at)

)

SELECT t1.month 
      ,t1.active_drivers
      ,NVL(t2.accepted_rides,0) as accepted_rides
FROM cte2 t1 
LEFT JOIN cte3 t2
ON t1.month = t2.month
ORDER BY t1.month
*/
