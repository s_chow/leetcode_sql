
/*
Table: Product

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| product_name  | varchar |
+---------------+---------+
product_id is the primary key for this table.
product_name is the name of the product.
 

Table: Sales

+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| product_id          | int     |
| period_start        | date    |
| period_end          | date    |
| average_daily_sales | int     |
+---------------------+---------+
product_id is the primary key for this table. 
period_start and period_end indicate the start and end date for the sales period, and both dates are inclusive.
The average_daily_sales column holds the average daily sales amount of the items for the period.
The dates of the sales years are between 2018 to 2020.
 

Write an SQL query to report the total sales amount of each item for each year, with corresponding product_name, product_id, product_name, and report_year.

Return the result table ordered by product_id and report_year.

The query result format is in the following example.

 

Example 1:

Input: 
Product table:
+------------+--------------+
| product_id | product_name |
+------------+--------------+
| 1          | LC Phone     |
| 2          | LC T-Shirt   |
| 3          | LC Keychain  |
+------------+--------------+
Sales table:
+------------+--------------+-------------+---------------------+
| product_id | period_start | period_end  | average_daily_sales |
+------------+--------------+-------------+---------------------+
| 1          | 2019-01-25   | 2019-02-28  | 100                 |
| 2          | 2018-12-01   | 2020-01-01  | 10                  |
| 3          | 2019-12-01   | 2020-01-31  | 1                   |
+------------+--------------+-------------+---------------------+
Output: 
+------------+--------------+-------------+--------------+
| product_id | product_name | report_year | total_amount |
+------------+--------------+-------------+--------------+
| 1          | LC Phone     |    2019     | 3500         |
| 2          | LC T-Shirt   |    2018     | 310          |
| 2          | LC T-Shirt   |    2019     | 3650         |
| 2          | LC T-Shirt   |    2020     | 10           |
| 3          | LC Keychain  |    2019     | 31           |
| 3          | LC Keychain  |    2020     | 31           |
+------------+--------------+-------------+--------------+
Explanation: 
LC Phone was sold for the period of 2019-01-25 to 2019-02-28, and there are 35 days for this period. Total amount 35*100 = 3500. 
LC T-shirt was sold for the period of 2018-12-01 to 2020-01-01, and there are 31, 365, 1 days for years 2018, 2019 and 2020 respectively.
LC Keychain was sold for the period of 2019-12-01 to 2020-01-31, and there are 31, 31 days for years 2019 and 2020 respectively.
*/


#  select level + 2017 as year from dual connect by level <= 3
#{"headers": ["YEAR"], "values": [[2018], [2019], [2020]]}

/* Write your PL/SQL query statement below */



with years as (
    select level + 2017 as year from dual connect by level <= 3
)

select  s.product_id, 
        p.product_name,
        to_char(y.year) as report_year,
        s.average_daily_sales * (least(to_date(year || '-12-31', 'YYYY-MM-DD'), s.period_end) 
                                - greatest(to_date(year || '-01-01', 'YYYY-MM-DD'), s.period_start) + 1)
                                as total_amount
from sales s join product p on s.product_id = p.product_id
         join years y on y.year between to_char(s.period_start, 'YYYY') and to_char(s.period_end, 'YYYY')
order by s.product_id, y.year;

/* 
SELECT a.product_id, b.product_name, a.report_year, a.total_amount
FROM (
    SELECT product_id, '2018' AS report_year,
        average_daily_sales * (DATEDIFF(LEAST(period_end, '2018-12-31'), GREATEST(period_start, '2018-01-01'))+1) AS total_amount
    FROM Sales
    WHERE YEAR(period_start)=2018 OR YEAR(period_end)=2018

    UNION ALL

    SELECT product_id, '2019' AS report_year,
        average_daily_sales * (DATEDIFF(LEAST(period_end, '2019-12-31'), GREATEST(period_start, '2019-01-01'))+1) AS total_amount
    FROM Sales
    WHERE YEAR(period_start)<=2019 AND YEAR(period_end)>=2019

    UNION ALL

    SELECT product_id, '2020' AS report_year,
        average_daily_sales * (DATEDIFF(LEAST(period_end, '2020-12-31'), GREATEST(period_start, '2020-01-01'))+1) AS total_amount
    FROM Sales
    WHERE YEAR(period_start)=2020 OR YEAR(period_end)=2020
) a
LEFT JOIN Product b
ON a.product_id = b.product_id
ORDER BY a.product_id, a.report_year

*/


/* 

with years as (
select level as year
  from dual
 where level >= (select min(to_char(period_start,'yyyy')) from sales)
 connect by level <= (select max(to_char(period_end,'yyyy')) from sales)
    )
, sales_split_by_year as (
select s.product_id,
       p.product_name,
       s.average_daily_sales,
       year,
       case when to_date(y.year ||'-01-01','yyyy-mm-dd') > 
                    period_start
            then to_date(y.year ||'-01-01','yyyy-mm-dd') 
            else period_start
        end as period_start,
        case when to_date(y.year ||'-12-31','yyyy-mm-dd') <
                    period_end
            then to_date(y.year ||'-12-31','yyyy-mm-dd') 
            else period_end
        end as period_end
  from sales s
  join product p
    on s.product_id = p.product_id
  join years y
    on y.year between to_char(period_start,'yyyy') and to_char(period_end,'yyyy')
order by s.product_id, y.year
    )

select sby.product_id,
       sby.product_name,
       to_char(sby.year) as report_year,
       --period_end - period_start as no_of_days,
       sby.average_daily_sales * (period_end - period_start + 1) as total_amount
  from sales_split_by_year sby
 order by product_id, report_year
 */