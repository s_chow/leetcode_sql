/*Input: 
Candidates table:
+-------------+------------+--------+
| employee_id | experience | salary |
+-------------+------------+--------+
| 1           | Junior     | 10000  |
| 9           | Junior     | 10000  |
| 2           | Senior     | 20000  |
| 11          | Senior     | 20000  |
| 13          | Senior     | 50000  |
| 4           | Junior     | 40000  |
+-------------+------------+--------+
Output: 
+------------+---------------------+
| experience | accepted_candidates |
+------------+---------------------+
| Senior     | 2                   |
| Junior     | 2                   |
+------------+---------------------+
Explanation: 
We can hire 2 seniors with IDs (2, 11). Since the budget is $70000 and the sum of their salaries is $40000, we still have $30000 but they are not enough to hire the senior candidate with ID 13.
We can hire 2 juniors with IDs (1, 9). Since the remaining budget is $30000 and the sum of their salaries is $20000, we still have $10000 but 
they are not enough to hire the junior candidate with ID 4.*/

with main_query as (
select 
employee_id, experience, salary, 
sum(salary) over (partition by experience order by experience desc, salary, employee_id) as cum_sum
from candidates) 

select 'Senior' as experience, count(*) as accepted_candidates from main_query where experience='Senior' and cum_sum <= 70000 
union
select 'Junior' as experience, count(*) as accepted_candidates from main_query where experience='Junior' and cum_sum <= 
    (select 70000-nvl(max(cum_sum),0) from main_query where experience='Senior' and cum_sum <= 70000) 






--Testing

/*  
select 
employee_id, experience, salary, 
sum(salary) over (partition by experience order by experience desc, salary) as cum
from candidates

{"headers": ["EMPLOYEE_ID", "EXPERIENCE", "SALARY", "CUM"], "values": 
[[1, "Junior", 10000, 20000], 
 [9, "Junior", 10000, 20000], 
 [4, "Junior", 40000, 60000], 
 [11, "Senior", 20000, 40000], 
 [2, "Senior", 20000, 40000], 
 [13, "Senior", 50000, 90000]]}

 select 
employee_id, experience, salary, 
sum(salary) over (partition by experience order by experience desc, salary, employee_id) as cum
from candidates

{"headers": ["EMPLOYEE_ID", "EXPERIENCE", "SALARY", "CUM"], "values":
[[1, "Junior", 10000, 10000], 
 [9, "Junior", 10000, 20000], 
 [4, "Junior", 40000, 60000], 
 [2, "Senior", 20000, 20000], 
 [11, "Senior",20000, 40000], 
 [13, "Senior", 50000, 90000]]}
 */