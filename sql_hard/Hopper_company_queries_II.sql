/* */

/* with ride as (
    select * from 
    (select rownum as month
    from dual 
    connect by rownum <= 12 ) m left join rides r on m.month = to_char(r.requested_at, 'MM')
    and to_char(r.requested_at, 'YYYY') = '2020'),
    
    accepteddrive as (
    select * from 
    drivers d left join acceptedrides a 
    on d.driver_id = a.driver_id)
    
    total as (
    select * from ride r left join accepteddrive a 
    on r.ride_id = a.ride_id where)
    */

    /* Write your PL/SQL query statement below */


with accepted_ride_2020 as (
    select a.ride_id, to_char(a.requested_at, 'MM') as month, b.driver_id
    from Rides a join AcceptedRides b
    on a.ride_id = b.ride_id
    where to_char(a.requested_at, 'YYYY') = '2020'
),

months_2020 as (
    select rownum as month, last_day(to_date(rownum, 'MM') - 365*2) as last_day_month
    from dual
    connect by rownum <= 12
),

ride_by_month as (
    select a.month,  b.driver_id as active_drivers, c.driver_id as working_drivers
    from months_2020 a left join drivers b
    on b.join_date <= a.last_day_month
    left join accepted_ride_2020 c
    on to_char(a.last_day_month, 'MM') = c.month
)

select month, 
       case when count(distinct active_drivers) = 0 then 0.00
            else round(100 * (count(distinct working_drivers) / count(distinct active_drivers)), 2)
            end as working_percentage
from ride_by_month
group by month

