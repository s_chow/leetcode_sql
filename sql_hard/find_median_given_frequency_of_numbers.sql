/*
Table: Numbers

+-------------+------+
| Column Name | Type |
+-------------+------+
| num         | int  |
| frequency   | int  |
+-------------+------+
num is the primary key for this table.
Each row of this table shows the frequency of a number in the database.
 

The median is the value separating the higher half from the lower half of a data sample.

Write an SQL query to report the median of all the numbers in the database after decompressing the Numbers table. Round the median to one decimal point.

The query result format is in the following example.

 

Example 1:

Input: 
Numbers table:
+-----+-----------+
| num | frequency |
+-----+-----------+
| 0   | 7         |
| 1   | 1         |
| 2   | 3         |
| 3   | 1         |
+-----+-----------+
Output: 
+--------+
| median |
+--------+
| 0.0    |
+--------+
Explanation: 
If we decompress the Numbers table, we will get [0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 3], so the median is (0 + 0) / 2 = 0.
*/

/*
sub query result



with num_cte as (
Select level rnk from dual connect by level <= (Select max(frequency) from numbers))
select *
from numbers join num_cte on (num_cte.rnk <= frequency)
order by 1

{"headers": ["NUM", "FREQUENCY", "RNK"], "values": 
[[0, 7, 5], 
 [0, 7, 4], 
 [0, 7, 1],
 [0, 7, 6], 
 [0, 7, 2], 
 [0, 7, 7], 
 [0, 7, 3], 
 
 [1, 1, 1], 
 
 [2, 3, 3], 
 [2, 3, 1], 
 [2, 3, 2], 
 
 [3, 1, 1]]}

*/

/* Write your PL/SQL query statement below */


with num_cte as (
Select level rnk from dual connect by level <= (Select max(frequency) from numbers))
select median(num) as median 
from numbers join num_cte on (num_cte.rnk <= frequency)
order by 1