/*
*/

/*
"headers": ["CALLER_ID", "RECIPIENT_ID", "FIRST_CALL", "LAST_CALL"], "values": 
[[5, 1, "2021-08-11 05:28:44", "2021-08-11 05:28:44"], 
 [8, 11, null, "2021-08-17 22:22:22"], 
 [8, 3, "2021-08-17 04:04:15", null], 
 [4, 8, null, "2021-08-24 19:57:13"], 
 [8, 4, "2021-08-24 17:46:07", null]]}*/

/* Write your PL/SQL query statement below */
/* 
my solution not working

with main_query as (
select * from (
    select caller_id, recipient_id ,  
    case when (call_time = min(call_time) over (partition by substr(call_time, 1, 10))) then call_time end as first_call,   
    case when (call_time = max(call_time) over (partition by substr(call_time, 1, 10))) then call_time end as last_call
    from calls ) a 
    where first_Call  is not null or last_call is not null) ,
with first_call as (
    select * from main_query where last_call is null
), 
with last_Call as (
    select * from main_query where first_call is null
)

select caller_id, recipient_id,  from 
first_Call f join last_call l where 
(f.caller_id = l.caller_id and f.recipient_id = l.recipient_id) or 
(f.caller_id = l.recipient_id and l.caller_id = f.recipient_id ) 

*/
WITH CTE AS (
                SELECT caller_id AS user_id, call_time, recipient_id FROM Calls
                UNION 
                SELECT recipient_id AS user_id, call_time, caller_id AS recipient_id FROM Calls
            ),

CTE1 AS (
        SELECT 
        user_id,
        recipient_id,
        substr(call_time, 1, 10) AS DAY,
        DENSE_RANK() OVER(PARTITION BY user_id, substr(call_time, 1, 10) ORDER BY call_time ASC) AS RN,
        DENSE_RANK() OVER(PARTITION BY user_id, substr(call_time, 1, 10) ORDER BY call_time DESC) AS RK
        FROM CTE
        )

SELECT DISTINCT user_id
FROM CTE1
WHERE RN = 1 OR RK = 1
GROUP BY user_id, DAY
HAVING COUNT(DISTINCT recipient_id) = 1

 /* 
 With all_Calls
  as (
select caller_id
      ,recipient_id
      ,call_time
      ,to_char(call_time, 'YYYY-MM-DD') call_day
  from Calls
 UNION ALL
select recipient_id
      ,caller_id
      ,call_time
      ,to_char(call_time, 'YYYY-MM-DD') call_day
  from Calls
      )
      
select distinct caller_id user_id
  from (
Select caller_id
      ,call_day
      ,first_value(recipient_id) over(partition by caller_id, call_day order by call_time rows between unbounded preceding and unbounded following) first_caller
      ,last_value(recipient_id) over(partition by caller_id, call_day order by call_time rows between unbounded preceding and unbounded following) last_caller
  from all_calls
 order by call_day
      )
where first_caller = last_caller
*/

/* 
with temp as (
select caller_id as user_id, recipient_id as to_id, call_time
from Calls
Union
select recipient_id as user_id, caller_id as to_id, call_time
from Calls
order by call_time
),

temp2 as (
select user_id, to_id, to_char(call_time, 'MM-DD-YYYY') as call_date,
dense_rank() over (partition by user_id, to_char(call_time, 'YYYY-MM-DD') order by call_time) as first,
dense_rank() over (partition by user_id, to_char(call_time, 'YYYY-MM-DD') order by call_time desc) as last
from temp
)

select distinct user_id
from temp2
where first = 1 or last = 1
group by user_id, call_date
having count(distinct to_id) = 1
*/

/*
I  see many solutions using FIRST_VALUE() in combination with DESC for accessing the last call. Probably because LAST_VALUE() did not work as expected.

The reason why LAST_VALUE() might not have worked, is because the last value in the current window is the current value (and not the last value in the partition). This window is implictly set by the ORDER BY clause - it sets the window to ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW).
To use LAST_VALUE() you need to extent the window to the full partition by using ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING.

with calls_augmented as (
    select caller_id as user_id_1, recipient_id as user_id_2, call_time from Calls
    union
    select recipient_id, caller_id, call_time from Calls
),
first_last_call as (
    select
        user_id_1 as user_id,
        first_value(user_id_2) over (partition by user_id_1, date(call_time) order by call_time) as first_call_id,
        last_value(user_id_2)  over (partition by user_id_1, date(call_time) order by call_time rows between unbounded preceding and unbounded following) as last_call_id
    from calls_augmented
)
select distinct user_id 
from first_last_call
where first_call_id = last_call_id
order by user_id*/

/* Mysql easy solution

with main_query as (
select * from (
    select caller_id, recipient_id ,  
    case when (call_time = min(call_time) over (partition by substr(call_time, 1, 10))) then call_time end as first_call,   
    case when (call_time = max(call_time) over (partition by substr(call_time, 1, 10))) then call_time end as last_call
    from calls ) a 
    where first_Call  is not null or last_call is not null) ,
with first_call as (
    select * from main_query where last_call is null
), 
with last_Call as (
    select * from main_query where first_call is null
)

select caller_id, recipient_id,  from 
first_Call f join last_call l where 
(f.caller_id = l.caller_id and f.recipient_id = l.recipient_id) or 
(f.caller_id = l.recipient_id and l.caller_id = f.recipient_id ) 

*/