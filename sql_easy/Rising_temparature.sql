/* 
Table: Weather

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| id            | int     |
| recordDate    | date    |
| temperature   | int     |
+---------------+---------+
id is the primary key for this table.
This table contains information about the temperature on a certain day.
 

Write an SQL query to find all dates' Id with higher temperatures compared to its previous dates (yesterday).

Return the result table in any order.

*/

Select Id from 
(select Id, 
 lag(Temperature) over (order by RecordDate) as ltemp, 
 Temperature as temp, 
 RecordDate, 
 lag(RecordDate) over (order by RecordDate) as lrd 
 from Weather)  
 where temp>ltemp and RecordDate-lrd=1 


