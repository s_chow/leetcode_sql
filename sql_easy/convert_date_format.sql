/* 
Table: Days

+-------------+------+
| Column Name | Type |
+-------------+------+
| day         | date |
+-------------+------+
day is the primary key for this table.
 

Write an SQL query to convert each date in Days into a string formatted as "day_name, month_name day, year".

Return the result table in any order.

*/

select to_char(day, 'FMDay') || ', '|| to_char(day, 'FMMonth DD, YYYY') as day from days