/* 
Table: Employee

+---------------+---------+
| Column Name   |  Type   |
+---------------+---------+
| employee_id   | int     |
| department_id | int     |
| primary_flag  | varchar |
+---------------+---------+
(employee_id, department_id) is the primary key for this table.
employee_id is the id of the employee.
department_id is the id of the department to which the employee belongs.
primary_flag is an ENUM of type ('Y', 'N'). If the flag is 'Y', the department is the primary department for the employee. If the flag is 'N', the department is not the primary.
 

Employees can belong to multiple departments. When the employee joins other departments, they need to decide which department is their primary department. Note that when an employee belongs to only one department, their primary column is 'N'.

Write an SQL query to report all the employees with their primary department. For employees who belong to one department, report their only department.

Return the result table in any order.

*/

select employee_id , department_id from employee where primary_flag = 'Y'
or employee_id in ( select distinct employee_id from employee 
group by employee_id
having count(*) = 1)

or 

SELECT EMPLOYEE_ID,department_id FROM
(
SELECT EMPLOYEE_ID,department_id, primary_flag, COUNT(employee_id) OVER(PARTITION BY EMPLOYEE_ID) AS COUNT_RECORDS FROM EMPLOYEE
) A where
A.COUNT_RECORDS = 1 OR A.PRIMARY_FLAG = 'Y';


or 
with cte as (
select employee_id,
department_id,
case when count(*)over(partition by employee_id) = 1 then 'Y' else primary_flag end as flag
from employee )

select employee_id,
department_id
from cte
where flag = 'Y'
