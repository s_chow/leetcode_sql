/*
Table: Cinema

+-------------+------+
| Column Name | Type |
+-------------+------+
| seat_id     | int  |
| free        | bool |
+-------------+------+
seat_id is an auto-increment primary key column for this table.
Each row of this table indicates whether the ith seat is free or not. 1 means free while 0 means occupied.
 

Write an SQL query to report all the consecutive available seats in the cinema.

Return the result table ordered by seat_id in ascending order.

The test cases are generated so that more than two seats are consecutively available.

*/

WITH tab1 as
(SELECT 
    seat_id, 
    free,
    LEAD(free) OVER(order by seat_id) AS next_flag,
    LAG(free) OVER(order by seat_id) AS prev_flag
FROM cinema
),
tab2 AS
(
SELECT 
    seat_id,
    free,
    CASE WHEN free = 1 and (next_flag = 1 or prev_flag = 1) THEN 'Consecutive' ELSE 'Non-consecutive' END AS cons
    FROM tab1
)
SELECT seat_id
FROM tab2
WHERE cons = 'Consecutive'