/* 
Table: Sessions

+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| session_id          | int     |
| duration            | int     |
+---------------------+---------+
session_id is the primary key for this table.
duration is the time in seconds that a user has visited the application.
 

You want to know how long a user visits your application. You decided to create bins of "[0-5>", "[5-10>", "[10-15>", and "15 minutes or more" and count the number of sessions on it.

Write an SQL query to report the (bin, total).
*/

SELECT '[0-5>' as bin, SUM(CASE WHEN duration/60 <= 5 THEN 1 else 0 end) as total FROM sessions
UNION
SELECT '[5-10>' as bin, SUM(CASE WHEN duration/60 > 5 AND duration/60 <= 10 THEN 1 else 0 end) as total FROM sessions
UNION
SELECT '[10-15>' as bin, SUM(CASE WHEN duration/60 > 10 AND duration/60 <= 15 THEN 1 else 0 end) as total FROM sessions
UNION
SELECT '15 or more' as bin, SUM(CASE WHEN duration/60 > 15 THEN 1 else 0 end) as total FROM sessions


