/*
Table: Problems

+-------------+------+
| Column Name | Type |
+-------------+------+
| problem_id  | int  |
| likes       | int  |
| dislikes    | int  |
+-------------+------+
problem_id is the primary key column for this table.
Each row of this table indicates the number of likes and dislikes for a LeetCode problem.
 

Write an SQL query to report the IDs of the low-quality problems. A LeetCode problem is low-quality if the like percentage of the problem (number of likes divided by the total number of votes) is strictly less than 60%.

Return the result table ordered by problem_id in ascending order.

*/

select problem_id from (
select problem_id, sum(likes)/sum(likes+dislikes) 
from 
problems 
group by problem_id
having sum(likes)/sum(likes+dislikes) <= 0.6 )
order by problem_id


/* or */

select problem_id
from 
problems 
group by problem_id
having sum(likes)/sum(likes+dislikes) <= 0.6
order by problem_id