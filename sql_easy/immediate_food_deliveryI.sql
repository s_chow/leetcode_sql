/* 
Table: Delivery

+-----------------------------+---------+
| Column Name                 | Type    |
+-----------------------------+---------+
| delivery_id                 | int     |
| customer_id                 | int     |
| order_date                  | date    |
| customer_pref_delivery_date | date    |
+-----------------------------+---------+
delivery_id is the primary key of this table.
The table holds information about food delivery to customers that make orders at some date and specify a preferred delivery date (on the same order date or after it).
 

If the customer's preferred delivery date is the same as the order date, then the order is called immediate; otherwise, it is called scheduled.

Write an SQL query to find the percentage of immediate orders in the table, rounded to 2 decimal places.
*/

select ROUND(
		AVG(case when order_date = customer_pref_delivery_date then 1 else 0 end) * 100,2
	) immediate_percentage from Delivery 
    
    
    select round(100*
    sum(case when customer_pref_delivery_date = order_date then 1 else 0 end)
    /count(*),2)
    as immediate_percentage
from delivery