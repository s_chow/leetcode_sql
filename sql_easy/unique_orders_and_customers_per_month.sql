/*Table: Orders

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| order_date    | date    |
| customer_id   | int     |
| invoice       | int     |
+---------------+---------+
order_id is the primary key for this table.
This table contains information about the orders made by customer_id.
 

Write an SQL query to find the number of unique orders and the number of unique customers with invoices > $20 for each different month.

Return the result table sorted in any order.*/

SELECT to_char(order_date, 'YYYY-MM') as month, 
       count(order_id) as order_count, 
	   count(distinct(customer_id)) as customer_count 
FROM Orders where invoice > 20
GROUP BY to_char(order_date, 'YYYY-MM')