
/* Table: Countries

+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| country_id    | int     |
| country_name  | varchar |
+---------------+---------+
country_id is the primary key for this table.
Each row of this table contains the ID and the name of one country.
 

Table: Weather

+---------------+------+
| Column Name   | Type |
+---------------+------+
| country_id    | int  |
| weather_state | int  |
| day           | date |
+---------------+------+
(country_id, day) is the primary key for this table.
Each row of this table indicates the weather state in a country for one day.
 

Write an SQL query to find the type of weather in each country for November 2019.

The type of weather is:

Cold if the average weather_state is less than or equal 15,
Hot if the average weather_state is greater than or equal to 25, and
Warm otherwise.
Return result table in any order.
*/

select 
country_name, 
case when av <= 15 then 'Cold'
     when av >= 25 then 'Hot'
     else 'Warm' end as weather_type from
(select country_name, avg(weather_state) as av from 
countries c , weather w
where c.country_id = w.country_id
 and to_char(w.day, 'YYYY-MM') = '2019-11'
group by country_name)